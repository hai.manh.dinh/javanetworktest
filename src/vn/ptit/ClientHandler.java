package vn.ptit;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ClientHandler extends Thread {
	private DataInputStream dataInputStream;
	private DataOutputStream dataOutputStream;
	private String name;
	private Socket socket;
	private boolean isLogin;

	public ClientHandler(DataInputStream dataInputStream, DataOutputStream dataOutputStream, String name,
			Socket socket) {
		super();
		this.dataInputStream = dataInputStream;
		this.dataOutputStream = dataOutputStream;
		this.name = name;
		this.socket = socket;
		this.isLogin = true;

	}

	@Override
	public void run() {
		String messageReceived;

		while (true) {
			try {
				messageReceived = dataInputStream.readUTF();
				System.out.println(messageReceived);
				if (messageReceived.equalsIgnoreCase("logout")) {
					isLogin = false;
					break;
				}

				for (ClientHandler clientHandler : Server.listClient) {
					if (!clientHandler.name.equalsIgnoreCase(name) && clientHandler.isLogin) {
						clientHandler.dataOutputStream.writeUTF(name + ": " + messageReceived);
					}
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		try {
			if (!Client.flag) {
				socket.close();
				dataOutputStream.close();
				dataInputStream.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
