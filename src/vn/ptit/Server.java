package vn.ptit;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {
	static List<ClientHandler> listClient = new ArrayList<>();
	static int i = 0;
	
	public static void main(String[] args) throws IOException {
		ServerSocket serverSocket = new ServerSocket(6969);
		
		Socket socket;
		
		while(true) {
			socket = serverSocket.accept();
			System.out.println("New client request received : "+socket);
			
			DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
			DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
			
			System.out.println("Creating a new handler for this client...");
			ClientHandler clientHandler = new ClientHandler(dataInputStream, dataOutputStream, "client"+i, socket);
			
			System.out.println("Adding this client to active client list");
			listClient.add(clientHandler);
			
			clientHandler.start();
			i++;	
		}
		
	}

}
