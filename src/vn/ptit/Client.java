package vn.ptit;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Client {

	static boolean flag = true;

	public static void main(String[] args) throws UnknownHostException, IOException {
		Scanner scanner = new Scanner(System.in);
		Socket socket = new Socket("localhost", 6969);

		DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
		DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());

		Thread sendMessage = new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					String msg = scanner.nextLine();
					try {
						dataOutputStream.writeUTF(msg);
						if (msg.equalsIgnoreCase("logout")) {
							flag = false;
							break;
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}

		});

		Thread readMessage = new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					try {
						if (!flag) {
							socket.close();
							dataInputStream.close();
							dataOutputStream.close();
							scanner.close();
							break;
						}
						String msg = dataInputStream.readUTF();
						System.out.println(msg);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}

		});

		sendMessage.start();
		readMessage.start();

	}

}
